<?php
// START: Added by Novem Designs, LLC - June/July 2017

/**  Child theme
 * The child theme that came with Mr. Tailor was not setup according to the way WordPress
 * recommends. The following function creates the child theme 'properly'.
 */
function nd_theme_enqueue_styles() {
  $parent_style = 'mrtailor-style';
  wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
  wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( $parent_style ), wp_get_theme()->get( 'Version' ) );
}
add_action( 'wp_enqueue_scripts', 'nd_theme_enqueue_styles' );

/** Adding 'Stock Left' after price
 * On product archive pages we want to include the number of items left in stock.
 * We ONLY show this is if:
 *     1. There are 10 or less items in stock
 *     2. The item stock value is 0 (sold out)
 *     3. The item is or is not manage BUT the 'Stock status' is set to 'Out of stock'
 */
function nd_woo_show_product_remaining( $price ) {
  // Variables for the text to display (to make it easier to edit for the client later)
  $out_of_stock_message = 'Sold out!';
  $left_in_stock_message = ' left in stock';

  global $product;

  // Set new variable to work with in case future adjustments require us to pass back the original $price variable
  $return_price = $price;

  // Check if we're managing the stock ('Manage Stock' checkbox is checked)
  if ( 'yes' == strtolower( $product->manage_stock ) ) {

    // Checking the number of items left in stock
    if ( number_format( $product->stock, 0, '', '' ) <= 0 ) {
      // 0 items left in stock
      $return_price = $price . '<span class="nd-product-stock-remaining nd-product-no-stock">' . $out_of_stock_message . '</span>';
    } elseif ( number_format( $product->stock, 0, '', '' ) > 0 && number_format( $product->stock, 0, '', '' ) <= 10 ) {
      // 0-10 items left in stock
      $return_price =  $price . '<span class="nd-product-stock-remaining nd-product-low-stock">Only ' . number_format( $product->stock, 0, '', '') . $left_in_stock_message . '</span>';
    } else {
      // 11+ items left in stock
      $return_price = $price;
    }
    
    // Special case of managing inventory, having stock, but 'Stock status' is set to 'Out of stock'
    if ( 'outofstock' == strtolower( $product->stock_status ) ) {
      $return_price = $price . '<span class="nd-product-stock-remaining nd-product-no-stock">' . $out_of_stock_message . '</span>';
    }


  } else {
    // We are NOT managing stock
    
    // We can have non-managed stock that is set to 'Out of stock'
    if ( 'outofstock' == strtolower( $product->stock_status ) ) {
      $return_price = $price . '<span class="nd-product-stock-remaining nd-product-no-stock">' . $out_of_stock_message . '</span>';
    }
  }

  return $return_price;
}
add_filter( 'woocommerce_get_price_html', 'nd_woo_show_product_remaining' );

// END: Added by Novem Designs, LLC - June/July 2017