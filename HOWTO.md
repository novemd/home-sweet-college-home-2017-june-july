# Home Sweet College Home - Child Theme, Template changes, and more

## Documentation for making changes

Megan, you asked if you would be able to make some changes to what we've setup for this child theme. I figured that it would be helpful for you to have a little visual guide on making some of these changes.

Below are a few of the changes you asked about and how you would make them.

* * *

# Changing the RED text color for 'Sold Out' on the product archive pages

* * *

First you'll need to edit the `style.css` file. On the WordPress menu select **Appearance > Editor** (see image below). 

![redtext](doc-imgs/hsch-red-text-1.png)

After you have clicked on **Editor** the `style.css` file should be loaded in the text editor. Near the top of the file you should look for a ruleset called `.nd-product-stock-remaining` (see image below). In there would will find a rule `color: #c00;` - it's this `#c00` that you will want to change.

![redtext](doc-imgs/hsch-red-text-2.png)

To change the color there are a lot of tools online for this -- for example: [This color picker](https://www.webpagefx.com/web-design/color-picker/).  You can use this tool to select a color you like. Then at the top there's a # followed by 6 hex characters. That would be the value you'd need. You'd replace the `#c00` with the color code you want to use. And make sure to click on the **Update File** button to save your changes.

**Note:** Make sure that you include the `#` symbol before the 6 character color code. 

* * *

# Changing the 'Sold Out' text for your products

* * *

In order to cover all your bases you'll need to change this in two areas. The first area is easy as it's part of your theme. To make this change you need to go to the **Shop** tab of your **Theme Options**

![soldouttext](doc-imgs/hsch-sold-out-1.png)

Then scroll to the bottom and look for the **Out of Stock Text** and change the text to what you want.

![soldouttext](doc-imgs/hsch-sold-out-2.png)

Then make sure you save your changes by clicking on the **Save Changes** button.

Next you need to make a change to the `functions.php` file.

On the WordPress menu select **Appearance > Editor** (see image below). 

![redtext](doc-imgs/hsch-red-text-1.png)

By default when you first come to the editor the `style.css` file will be loaded. On the right side of the screen you will see a list of other files. Click on the `functions.php` file.

![soldouttext](doc-imgs/hsch-sold-out-2.png)

In this file, near the top, you will see a section with two variables. You want to change the value of the `$out_of_stock_message` variable (#1 in the image below). This value should be in single or double quote and followed by a semi-colon.

![soldouttext](doc-imgs/hsch-stock-message-2.png)

**IMPORTANT NOTE: Making an error in this file can cause issues with your sight. The `functions.php` file is critical to your theme and an error here may render your site useless until the error in this file is corrected. If you're not comfortable with making changes here I would HIGHLY recommend getting some help.**

* * *

# Changing the 'Sold Out' and 'left in stock' text for your products

* * *

We covered how to change the **'Sold Out'** text in the section above. But if you wanted to change the **'left in stock'** message you can do so by editing the `functions.php` file.

On the WordPress menu select **Appearance > Editor** (see image below). 

![redtext](doc-imgs/hsch-red-text-1.png)

By default when you first come to the editor the `style.css` file will be loaded. On the right side of the screen you will see a list of other files. Click on the `functions.php` file.

![soldouttext](doc-imgs/hsch-sold-out-2.png)

In this file, near the top, you will see a section with two variables. You want to change the value of the `$left_in_stock_message` variable (#2 in the image below). This value should be in single or double quote and followed by a semi-colon.

![soldouttext](doc-imgs/hsch-stock-message-2.png)

**Note: ** You'll notice the text value in this variable has a space between the opening apostrophe and the start of the text. That's because we're showing a number before this text. Without that space the text displayed on your site would look like **8left in stock** where we want it to show **8 left in stock**.

**IMPORTANT NOTE: Making an error in this file can cause issues with your sight. The `functions.php` file is critical to your theme and an error here may render your site useless until the error in this file is corrected. If you're not comfortable with making changes here I would HIGHLY recommend getting some help.**

* * *

# Adding Tabs to your products

* * *

There are two plugins installed and setup for you to make use of using tabs for your product descriptions. I also created a product for you (set as a draft) called **Using Tabs - Demo**. This is meant to be used as a reference for you should you need it.

![tabdemo](doc-imgs/hsch-tab-draft-1.png)

When you go to add the tabs shortcode to your **Product short description** section, I highly recommend that you do so while using the **Text** tab of the editor.

![tabdemo](doc-imgs/hsch-tab-draft-2.png)

The plugin we are using for these tabs is called [Tabs Shortcode](https://wordpress.org/plugins/tabs-shortcode/). You can read more about it on its WordPress Plugin page - the documentation on how to use it is there are well.

A short example would be:

```HTML
[tabs]
[tab title="Message"]
Finals are stressful. Encourage your student to take a break with this fun care package.
[/tab]
[tab title="Includes"]
<ul>
 	<li>Adult Coloring Book</li>
 	<li>12 Pack of Pencils</li>
 	<li>Star Shaped Stress Ball</li>
 	<li>Tea (2 Packs)</li>
 	<li>eos Hand Lotion</li>
 	<li>Trident Gum</li>
</ul>
[/tab]
[tab title="Shipping"]
<em>Products ship same day if ordered before 5pm, products will ship next day if ordered after 5pm.</em>
[/tab]
[/tabs]

```

* * *

# Changing the text for Upsells on the individual product pages

* * *

We've change the text on the individual product pages for upsells from 'You May Also Like' to 'Suggested Add Ons'. This was done using jQuery placed into the **Custom Code** section of your **Theme Options**

To make a change to this text you need to go to your **Theme Options** and click on the **Custom Code** tab.

![upsell](doc-imgs/hsch-upsell-1.png)

Here you will see a section called **Footer JavaScript Code** where there is currently only one line of code.

![upsell](doc-imgs/hsch-upsell-2.png)

To make a change you would simple change the `Suggested Add Ons` to the text that you would like to use and then save the changes.


**Documentation by [Novem Designs, LLC](http://www.novemwebdesign.com) and Dan Gronitz**