# Home Sweet College Home - Child Theme, Template changes, and more

**Date:** June 22, 2017 - (approx) July 11, 2017

**For:** Megan Sweere - Home Sweet College Home


## Tasks (direct copy from contract)

1. Creating a child theme
2. Product page (product category list page) -- add the number of items in stock for the product (under the product name and price)
3. Product page (product category list page) -- center the text for the product name/price/# of items in inventory - if difficult this isn't necessary
4. Individual product pages -- See if we're able to use some shortcode to make tabs for the content. And if that doesn't work then we'd have to rewrite the page template to handle the layout.
5. Change "Out of Stock" to "Sold Out" for products.
6. Arrange product 'add-ons' under the main product offering with product pictures, name, price, quantity, and add to cart button. I think using the up-sells built in function of woo commerce will work, I would just like to move those choices up under the main product picture and change the wording from "you may also like..." to "Suggested Add Ons".



### Solutions

#### Task 1

1. The theme (Mr. Tailor) came with a child theme but it was not following WordPress recommendations. Corrected the issues to load the parent theme's `style.css` file correctly - and - created a `functions.php` file as there was none with the child theme.

#### Task 2

1. In the `functions.php` file I added a filter to the WooCommerce hook `woocommerce_get_price_html` that would add the number of stock items in a `span` tag (for styling reasons).

#### Task 3

1. Added CSS styles to the `styles.css` file. Changes are documented in this file.

#### Task 4

1. As this theme removes and add their own names for WooCommerce filters I needed to relie on two plugins.
2. The first plugin is called [Advanced Excerpt](https://wordpress.org/plugins/advanced-excerpt/) and was needed to allow shortcodes to be used in the WooCommerce product short description area.
3. The second plugin is called [Tabs Shortcode](https://wordpress.org/plugins/tabs-shortcode/) and allows the client to use shortcode to build out the tabs for the products.

#### Task 5

1. This theme has a theme option to change the out of stock text. Theme Options > Shop (tab) > Out of Stock Text (area)

#### Task 6

1. Copied over the WooCommerce templates from the parent theme. All editing was done on to the `content-single-product.php` file.
2. Copied the code at the bottom of this file for WooCommerce Up-Sells and placed it under the main product image.
3. On mobile this location would break the flow of the one column layout. It would go: Main Product Image > Up Sells > Product Description. To fix this I do two things: (1) In a two column layout the Up Sells at the bottom are hidden with CSS and we show them under the main product image. (2) For one column layout we show the original Up Sells at the bottom and hide them under the main product image.
4. 'You man also like' changed to 'Suggested Add Ons' using jQuery placed in the theme options 'Custom Code' section.


**Plugin created and maintained by [Novem Designs, LLC](http://www.novemwebdesign.com) and Dan Gronitz**